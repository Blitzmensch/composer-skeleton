<?php
/**
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Yokoo\Skeleton\Tests\Exception;

use Yokoo\Skeleton\Exception\ExceptionInterface;

/**
 * @since   0.1.0
 */
class ExceptionInterfaceTest extends \PHPUnit_Framework_TestCase
{
    protected $interface = null;

    protected function setUp()
    {
        $this->interface = ExceptionInterface::class;
    }

    public function testExceptionInterfaceExists()
    {
        $this->assertTrue(interface_exists($this->interface), sprintf('interface %s does not exist', $this->interface));
    }
}
