<?php
/**
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace Yokoo\Skeleton\Tests\Exception;

use Exception as SplException;
use Yokoo\Skeleton\Exception\Exception;
use Yokoo\Skeleton\Exception\ExceptionInterface;

/**
 * @since   0.1.0
 */
class ExceptionTest extends \PHPUnit_Framework_TestCase
{
    protected $exception = null;

    protected function setUp()
    {
        $this->exception = new Exception();
    }

    public function testExceptionIsThrowable()
    {
        $this->assertInstanceOf(SplException::class, $this->exception);
    }

    public function testExceptionImplementsExceptionInterface()
    {
        $this->assertInstanceOf(ExceptionInterface::class, $this->exception);
    }
}
