<?php
/**
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

/**
 * Set default timezone
 */
date_default_timezone_set('UTC');

/**
 * Bootstrap autoloader
 */
$includeIfExists = function ($file) {
    return is_file($file) ? include $file : false;
};

/**
 * Try to initialize the autoloader
 */
if (!$loader = $includeIfExists(__DIR__ . '/vendor/autoload.php')) {
    exit(1);
}
